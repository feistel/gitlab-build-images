#!/bin/bash
# This script attempts to copy the latest version of the Google Chrome Debian
# package into our own S3 bucket. Google yanks old versions regularly, making
# it hard to keep up with all the new versions.

set -e

if [[ -z $AWS_ACCESS_KEY_ID || -z $AWS_SECRET_ACCESS_KEY ]]; then
    echo "AWS credentials are not defined, skipping this step"
    exit 0
fi

CHROME_S3_BUCKET=${1:-gitlab-google-chrome-stable}

export DEBIAN_FRONTEND=noninteractive
apt-get -y update
apt-get -y install apt-utils curl awscli

curl -sS -L https://dl.google.com/linux/linux_signing_key.pub | apt-key add -
echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list

echo "Updating apt to get Google Chrome packages..."

apt-get -y -q update

echo "Checking for latest Chrome version in apt repository..."

LATEST_VERSION=$(apt-cache show google-chrome-stable | grep Version | sort | tail -1 | sed -e "s/Version: //")
CHROME_DEB="google-chrome-stable_${LATEST_VERSION}_amd64.deb"
CHROME_URL="https://s3.amazonaws.com/gitlab-google-chrome-stable/${CHROME_DEB}"

echo "Checking if cache has $CHROME_DEB"
FILE_CHECK=$(curl -sL -I -w "%{http_code}\\n" "$CHROME_URL" -o /dev/null)

if [ "$FILE_CHECK" -eq "200" ]; then
    echo "Latest version $LATEST_VERSION is already cached!"
else
    apt-get -d -y install google-chrome-stable
    echo "Transfering $CHROME_DEB to S3 cache"
    aws s3 cp /var/cache/apt/archives/$CHROME_DEB s3://$CHROME_S3_BUCKET
fi
